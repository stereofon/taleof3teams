VAR wichtiger = "ungewiss"
VAR unwichtiger = "ungewiss"
VAR zielgewicht = "ungewiss"
VAR prodgewicht = "ungewiss"


Es waren einmal drei Teams. Sie liebten einander, aber wussten nicht so recht, wie sie zueinander finden sollten. Sie machten sich auf den Weg zu Prinzessin Strukturella und fragen sie um Rat.
Ihre erste Frage war: Was verbindet Euch mehr?
*   Ein gemeinsames Ziel 
    ~ wichtiger = "gemeinsames Ziel"
    ~ unwichtiger = "gemeinsame Arbeit"
    -> zielwieviel
*   Die t&auml;gliche Arbeit. 
    ~ wichtiger = "gemeinsame Arbeit"
    ~ unwichtiger = "gemeinsames Ziel"
    -> prodwieviel

== zielwieviel ==

Ein klein wenig z&ouml;gerte die Prinzessin und fragte sich, wie gross die &Uuml;berschneidung des gemeinsamen Zieles wohl ist.
*   Voll 
    ~ zielgewicht = "voll"
    -> zielvoll
*   Ziemlich. 
    ~ zielgewicht = "halb"
    -> zielhalb
*   Nicht. 
    ~ zielgewicht = "nicht"
    -> zielnicht

== prodwieviel ==
ein klein wenig z&ouml;gerte die Prinzessin und fragte sich, wie gross die Ueberschneidung der gemeinsamen Arbeit wohl ist.
*   voll 
    ~ prodgewicht = "voll"
    -> prodvoll
*   halb 
    ~ prodgewicht = "halb"
    -> prodhalb
*   nicht 
    ~ prodgewicht = "nicht"
    -> prodnicht

== zielvoll

 und ganz stehen wir hinter _einem_  gemeinsamen Ziel.


-> ziel_prodwieviel

== zielhalb

Die Leute schauten sich verwundert in die Augen, wollten sie doch eigentlich eine volle &Uuml;berschneidung. Miteinander reflektierten sie dar&uuml;ber und kamen zum Schluss, dass die &Uuml;berschneidung doch nur *halb* ist. 
-> ziel_prodwieviel

==  zielnicht

Eigentlich kam man zu dem Schluss, dass ein gemeinsames Ziel nach einer l&auml;ngeren Diskussion doch nicht exisitert. Demnach wollten sie nun die gemeinsame Arbeit beleuchten.
-> ziel_prodwieviel



== prodvoll
-> prod_zielwieviel
== prodhalb
Bei nur halber &Uuml;berschneidung erschien es erst verwunderlich, dass die t&auml;gliche Arbeit wichtiger erschien. Dennoch gewichteten sie die t&auml;gliche Arbeit mehr und fragten nun nach dem gemeinsamen Ziel, auf das die halbe Arbeit einzahlen sollte.
-> prod_zielwieviel
==  prodnicht
-> prod_zielwieviel

== prod_zielwieviel
Wichtiger ist also ein {wichtiger} und zwar {prodgewicht}. Dennoch war die &Uuml;berschneidung des Zieles von Interesse. Die Menschen antworteten es vereint sie:
*   Voll 
    ~ zielgewicht = "voll"
    -> schluss
*   Ziemlich. 
    ~ zielgewicht = "halb"
    -> schluss
*   Nicht. 
    ~ zielgewicht = "nicht"
    -> schluss


== ziel_prodwieviel

Wichtiger ist also ein {wichtiger} und zwar {zielgewicht}. Dennoch moechte jede die Verbindung zum Ziel in der t&auml;glichen Arbeit sp&uuml;hren. Unabhaengig davon fragt die junge Weise unsere Besucher, in welchem Grade ist denn Eure Arbeit voneinander abhaengig?

*   voll 
    ~ prodgewicht = "voll"
    -> schluss
*   halb 
    ~ prodgewicht = "halb"
    -> schluss
*   nicht 
    ~ prodgewicht = "nicht"
    -> schluss

-> END

== schluss
Am Ende einigten sich alle Beteiligten zu einem Experiment, was zu mehr Klarheit f&uuml;hren kann. Denn {wichtiger} ging ueber {unwichtiger}.

Das Experiment enthielt verschiedene Events zu denen eingeladen wurde. Ein { wichtiger == "gemeinsames Ziel": Regelmaessiges Governance Meeting zum gemeinsamen Entscheidungen treffen | Planning Event mit gemeinsamen Backlog} sollte { (wichtiger == "gemeinsames Ziel") and (zielgewicht == "voll"): in w&ouml;chentlichen Abst&auml;nden gehalten werden.}{ (wichtiger == "gemeinsames Ziel") and (zielgewicht == "halb"): in monatlichen Abst&auml;nden gehalten werden.}{ (wichtiger == "gemeinsames Ziel") and (zielgewicht == "nicht"): in Abst&auml;nden nach Bedarf gehalten werden.}{ (wichtiger == "gemeinsame Arbeit") and (prodgewicht == "voll"): in w&ouml;chentlichen Abst&auml;nden gehalten werden.}{ (wichtiger == "gemeinsame Arbeit") and (prodgewicht == "halb"): in monatlichen Abst&auml;nden gehalten werden.}{ (wichtiger == "gemeinsame Arbeit") and (prodgewicht == "nicht"): in Abst&auml;nden nach Bedarf gehalten werden.}

Dennoch empfahl sie auch f&uuml;r die unwichtigere {unwichtiger} ein { wichtiger == "gemeinsames Ziel": Planning Event mit gemeinsamen Backlog | Regelmaessiges Governance Meeting zum gemeinsamen Entscheidungen treffen }. Dies sollte { (wichtiger == "gemeinsame Arbeit") and (zielgewicht == "voll"): in w&ouml;chentlichen Abst&auml;nden gehalten werden.}{ (wichtiger == "gemeinsame Arbeit") and (zielgewicht == "halb"): in monatlichen Abst&auml;nden gehalten werden.}{ (wichtiger == "gemeinsame Arbeit") and (zielgewicht == "nicht"): in Abst&auml;nden nach Bedarf gehalten werden.}{ (wichtiger == "gemeinsames Ziel") and (prodgewicht == "voll"): in w&ouml;chentlichen Abst&auml;nden gehalten werden.}{ (wichtiger == "gemeinsames Ziel") and (prodgewicht == "halb"): in monatlichen Abst&auml;nden gehalten werden.}{ (wichtiger == "gemeinsames Ziel") and (prodgewicht == "nicht"): in Abst&auml;nden nach Bedarf gehalten werden.}

und auf diese Weise trafen Sie sich gl&uuml;cklich f&uuml;r eine Dauer von zwei Monaten, danach wollten sie das Vorgehen nocheinmal gemeinsam beleuchten und an ihre ver&auml;nderten Bed&uuml;rfnisse anpassen.

-> END
